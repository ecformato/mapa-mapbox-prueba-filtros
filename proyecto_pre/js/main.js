import * as d3 from 'd3';
import '../css/main.scss';

/* VARIABLES DE COLORES (primer color = + claro; segundo color = + oscuro) */
let independentistas_colors = ['#f6f7b3','#e0e400'], 
    no_independentistas_colors = ['#e7e7e7','#000'], 
    catcomu_podem_colors = ['#d7a7dd','#692772'], 
    cs_colors = ['#ffc09f','#f45a09'],
    cup_colors = ['#fcf9ca','#fff200'],
    erc_colors = ['#fde0af','#f9b33c'],
    junts_colors = ['#bae8e4','#20c0b2'],
    // pdcat_colors = ['#8daacd','#16447b'],
    // pnc_colors = ['#a5d0db','#2da8b8'],
    pp_colors = ['#c6e5ff','#48aafd'],
    psc_colors = ['#fbc9c7','#f60b01'],
    // vox_colors = ['#d5d9c0','#77be2d'],
    // pumj_colors = ['#d6e3e7','#7ec2d6'],
    // pacma_colors = ['#dbdebc','#adbd22'],
    // dialeg_colors = ['#b1d7ca','#27a87c'],
    // recortes_0_colors = ['#c8c8c8','#575757'],
    empate = '#e2e2d8';

mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syMGI5NHV4MDB1OTNnbnJtZ3UweGh5YiJ9.UrXbe3ehqGCgKdgMgHZnBQ';

/* Ajustar ZOOM, MINZOOM Y CENTER EN FUNCIÓN DEL DISPOSITIVO */
let mapWidth = document.getElementById('mapa').clientWidth;
let zoom = mapWidth > 425 ? 6.75 : 6.15;
let minZoom = 6;
let center = [1.75, 41.75];
let hoveredStateId = null;

let map = new mapboxgl.Map({
    container: 'mapa',
    style: 'mapbox://styles/datos-elconfidencial/ckipqslkk0fbe17oi4prebjmq',
    attributionControl: false,
    zoom: zoom,
    minZoom: minZoom,
    maxZoom: 14,
    center: center
});

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        language: 'ES',
        country: 'ES-CT*',
        marker: false,
        placeholder: 'Busque por calle o municipio'
    })
);

/* Variable para poder navegar sobre el mapa */
let nav = new mapboxgl.NavigationControl();
map.addControl(nav);

/* Inicializar variable para trabajar con popups */
let popup = new mapboxgl.Popup({
    closeButton: true,
    closeOnClick: false
});



map.scrollZoom.disable();

map.on('load', function(){
    let file = 'https://raw.githubusercontent.com/CarlosMunozDiaz/prueba-catalunya/main/csv_censal_2017.csv';
    d3.text(file, function(data){        
        map.addSource('secciones_censales', {
            'type': 'vector',
            'url': 'mapbox://datos-elconfidencial.8g4qnsfk',
            promoteId: 'CUSEC'
        });
        let dsv = d3.dsvFormat(";");
        let parseData = dsv.parse(data);
        
        parseData.map((item) => {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_v2-2970yn',
                id: item['Seccion_completo']
            }, {
                //Fuerzas políticas (y porcentajes)
                primeraFuerza: item['PrimeraF'].trim(),
                primeraFuerzaPorc: +item['PrimeraF_P'].replace(',','.'),
                segundaFuerza: item['SegundaF'].trim(),
                segundaFuerzaPorc: +item['SegundaF_P'].replace(',','.'),

                //Bloques
                bloque_independencia: item['Bloque_gana'].trim(),
                bloque_inde_inde_porc: +item['Independentistas'].replace(',','.'),
                bloque_inde_consti_porc: +item['No independentistas'].replace(',','.'),

                //Partidos
                junts_porc: +item['JXCat_P'].replace(',','.'),
                erc_porc: +item['ERC_P'].replace(',','.'),
                cs_porc: +item['Cs_P'].replace(',','.'),
                podem_porc: +item['CatComú_P'].replace(',','.'),
                psc_porc: +item['PSC_P'].replace(',','.'),
                pp_porc: +item['PP_P'].replace(',','.'),
                cup_porc: +item['CUP_P'].replace(',','.'),
            
                //Otros
                participacion: +item['Part_porc'].replace(',','.')
            });
        });        

        //Coropletas > Primero rellenamos con ganador
        map.addLayer({
            'id': 'secciones_censales_cat',
            'source': 'secciones_censales',
            'source-layer': 'secciones_v2-2970yn',
            'type': 'fill',
            'paint': {
                'fill-color': [
                    'match',
                    ['feature-state', 'primeraFuerza'],
                    "JUNTSxCAT", junts_colors[1],
                    "C's", cs_colors[1],
                    "ERC-Cat Sí", erc_colors[1],
                    "PSC", psc_colors[1],
                    "PP", pp_colors[1],
                    "CUP", cup_colors[1],
                    "CatComú-Podem", catcomu_podem_colors[1],
                    empate
                ]
            }      
        });

        //Límites censales
        map.addLayer({
            'id': 'secciones_censales_line',
            'source': 'secciones_censales',
            'source-layer': 'secciones_v2-2970yn',
            'type': 'line',
            'paint': {
                'line-color': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    'black',
                    'grey'
                ],
                'line-width': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    2,
                    .1
                ]
            }      
        });

        /* 
        * Eventos con botones 
        */
        document.getElementById('primeraFuerza').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--primera-fuerza');
            handlePrimera();
        });

        document.getElementById('segundaFuerza').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--segunda-fuerza');
            handleSegunda();
        });

        document.getElementById('independencia').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--independentismo');
            handleIndependencia();
        });

        document.getElementById('participacion').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--participacion');
            handleParticipacion();
        });

        document.getElementById('juntsPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--junts');
            handleJunts();
        });

        document.getElementById('ercPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--erc');
            handleErc();
        });

        document.getElementById('pscPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--psc');
            handlePsc();
        });

        document.getElementById('csPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--cs');
            handleCs();
        });

        document.getElementById('ppPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--pp');
            handlePp();
        });

        document.getElementById('podemPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--podem');
            handlePodem();
        });

        document.getElementById('cupPorcentajes').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend('legend--cup');
            handleCup();
        });

        function handlePrimera() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ['match',
                ['feature-state', 'primeraFuerza'],
                "JUNTSxCAT", junts_colors[1],
                "C's", cs_colors[1],
                "ERC-Cat Sí", erc_colors[1],
                "PSC", psc_colors[1],
                "PP", pp_colors[1],
                "CUP", cup_colors[1],
                "CatComú-Podem", catcomu_podem_colors[1],
                empate
            ]);
        }

        function handleSegunda() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ['match',
                ['feature-state', 'segundaFuerza'],
                "JUNTSxCAT", junts_colors[1],
                "C's", cs_colors[1],
                "ERC-Cat Sí", erc_colors[1],
                "PSC", psc_colors[1],
                "PP", pp_colors[1],
                "CUP", cup_colors[1],
                "CatComú-Podem", catcomu_podem_colors[1],
                empate
            ]);
        }

        function handleIndependencia() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ['match',
                ['feature-state', 'bloque_independencia'],
                "Independentistas", [
                    "interpolate", [
                        "linear", 1
                    ],
                    ['feature-state', 'bloque_inde_inde_porc'],
                    50, independentistas_colors[0],
                    100, independentistas_colors[1]
                ],
                "No independentistas", [
                    "interpolate", [
                        "linear", 1
                    ],
                    ['feature-state', 'bloque_inde_consti_porc'],
                    50, no_independentistas_colors[0],
                    100, no_independentistas_colors[1]
                ],
                empate
            ]);
        }

        function handleParticipacion() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'participacion'],
                    60, '#e7e7e7',
                    100, '#000'
                ]
            );
        }

        function handleJunts() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'junts_porc'],
                    0, junts_colors[0],
                    70, junts_colors[1]
                ]
            );
        }

        function handleErc() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'erc_porc'],
                    0, erc_colors[0],
                    70, erc_colors[1]
                ]
            );
        }

        function handlePsc() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'psc_porc'],
                    0, psc_colors[0],
                    60, psc_colors[1]
                ]
            );
        }

        function handleCs() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'cs_porc'],
                    0, cs_colors[0],
                    60, cs_colors[1]
                ]
            );
        }

        function handlePp() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'pp_porc'],
                    0, pp_colors[0],
                    30, pp_colors[1]
                ]
            );
        }

        function handleCup() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'cup_porc'],
                    0, cup_colors[0],
                    30, cup_colors[1]
                ]
            );
        }

        function handlePodem() {
            map.setPaintProperty('secciones_censales_cat', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'podem_porc'],
                    0, catcomu_podem_colors[0],
                    30, catcomu_podem_colors[1]
                ]
            );
        }

        //Eventos de ratón
        bind_event(popup, 'secciones_censales_cat');   
    });
});

//Uso del tooltip
function bind_event(popup, id){
    map.on('mousemove', id, function(e){
        //Primera parte
        let propiedades = e.features[0];
        map.getCanvas().style.cursor = 'pointer';
        var coordinates = e.lngLat;        
        var tooltipText = get_tooltip_text(propiedades);
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }        
        popup.setLngLat(coordinates)
            .setHTML(tooltipText)
            .addTo(map);

        //Segunda parte
        if (e.features.length > 0) {
            if (hoveredStateId) {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_v2-2970yn',
                id: hoveredStateId
            }, {
                hover: false
            });
            }
            hoveredStateId = e.features[0].id;
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_v2-2970yn',
                id: hoveredStateId
            }, {
                hover: true
            });
        }
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', id, function() {
        map.getCanvas().style.cursor = '';
        popup.remove();

        if (hoveredStateId) {
        map.setFeatureState({
            source: 'secciones_censales',
            sourceLayer: 'secciones_v2-2970yn',
            id: hoveredStateId
        }, {
            hover: false
        });
        }
        hoveredStateId = null;
    });
}  

function get_tooltip_text(propiedades){
    let caracteristicas_fijas = propiedades.properties;
    let estado = propiedades.state;
    
    let html = '';
    html = `<p><b>${caracteristicas_fijas.NMUN} (Participación: ${estado.participacion.toFixed(2)}%)</b></p>
            <p>Ganador: ${estado.primeraFuerza}</p>
            <p>Segundo: ${estado.segundaFuerza}</p>
            <p>Ganan ${estado.bloque_independencia}</p>
            <p><b>% indepe.: ${estado.bloque_inde_inde_porc} vs % no : ${estado.bloque_inde_consti_porc}</b></p>
            <hr>
            <p>Junts: ${estado.junts_porc}%</p>
            <p>ERC: ${estado.erc_porc}%</p>
            <p>Cs: ${estado.cs_porc}%</p>
            <p>PSC: ${estado.psc_porc}%</p>
            <p>PP: ${estado.pp_porc}%</p>
            <p>CatComú Podem: ${estado.podem_porc}%</p>
            <p>CUP: ${estado.cup_porc}%</p>`;
    
    return html; 
}

//Cambio botones
let buttons = document.getElementsByClassName('filters')[0];
let currentButton = buttons.getElementsByClassName('selected')[0];

function changeButton(button){
    currentButton.classList.remove('selected');
    button.classList.add('selected');
    currentButton = button;
}


//Cambio leyendas
let legends = document.getElementsByClassName('legends')[0];
let currentLegend = legends.getElementsByClassName('selected')[0];

function changeLegend(legend){
    currentLegend.classList.remove('selected');
    let aux = legends.getElementsByClassName(`${legend}`)[0];
    aux.classList.add('selected');
    currentLegend = aux;
}